package com.example.TestApiResponses;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TestApiResponsesApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void positiveGetFileBatchTest() throws Exception{
        Properties properties = new Properties();
		properties.load(TestApiResponsesApplication.class.getResourceAsStream("/application.properties"));

		String uri1 = properties.getProperty("uri1");
		Stream<String> all_lines = Files.lines(Paths.get(uri1));
        List<Object> data = new ArrayList<Object>();
        List<Object> data2 = new ArrayList<Object>();
        data = all_lines.skip(0).limit(0).collect(Collectors.toList());
        assertEquals(data2, data);
	}

}
