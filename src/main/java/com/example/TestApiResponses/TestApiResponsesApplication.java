package com.example.TestApiResponses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestApiResponsesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApiResponsesApplication.class, args);
	}

}
